#include "sort.h"
#include <time.h>

static int p(int A[], int start, int end)
{
    // choose the last one as p
    int mid = A[end];
    int i = start;
    int j = end - 1;
    while (1)
    {
        while (i < end && A[i] <= mid) i++;
        while (j >= start && A[j] >= mid) j--;
        if (i < j)
        {
            int tmp = A[i];
            A[i] = A[j];
            A[j] = tmp;
        }
        else
		{
			int tmp = A[end];
			A[end] = A[i];
			A[i] = tmp;
			return i;
		}
    }
}

static int p1(int A[], int start, int end)
{
    // choose random number as p
    int _mid = rand() % (end-start) + start;

    int mid = A[_mid]; 
    int i = start;
    int j = end - 1;

    // swap with the last one
    my_swap(&A[_mid], &A[end]);

    // go
    while (1)
    {
        while (i < end && A[i] <= mid) i++; // 
        while (j >= start && A[j] >= mid) j--;
        if (i < j)
        {
            int tmp = A[i];
            A[i] = A[j];
            A[j] = tmp;
        }
        else
		{
			int tmp = A[end];
			A[end] = A[i];
			A[i] = tmp;
			return i;
		}
    }
}

static void _quick_sort(int A[], int start, int end)
{
	if ((end - start) <= 0) return ;
	int q = p1(A, start, end);
	_quick_sort(A, start, q - 1);
	_quick_sort(A, q + 1, end);
}

void quick_sort(int A[], int size)
{
    srand(time(NULL));
	_quick_sort(A, 0, size - 1);
}
