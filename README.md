MIT Introduction to Algorithms
===============================

第一部分（Part I） 基础（Foundations）　　
-------------------------------------------------------------------------------------

* 第一章 计算中算法的角色（The Role of Algorithms in Computing）
* 第二章 开始（Getting Started）
* 第三章 函数的增长率（Growth of Functions）
* 第四章 递归（Recurrences）
* 第五章 概率分析与随机化算法（Probabilistic Analysis and Randomized Algorithms）

第二部分（Part II） 排序与顺序统计（Sorting and Order Statistics）(1)
-------------------------------------------------------------------------------------

* 第六章 堆排序（Heapsort）
* 第七章 快速排序（Quicksort）
* 第八章 线性时间中的排序（Sorting in Linear Time）
* 第九章 中值与顺序统计（Medians and Order Statistics）

第三部分（Part III） 数据结构（Data Structures）(2)
-------------------------------------------------------------------------------------

* 第十章 基本的数据结构（Elementary Data Structures）
* 第十一章 散列表（Hash Tables）
* 第十二章 二叉查找树（Binary Search Trees）
* 第十三章 红-黑树（Red-Black Trees）
* 第十四章 扩充的数据结构（Augmenting Data Structures）

第四部分（Part IV） 高级的设计与分析技术（Advanced Design and Analysis Techniques) (3)
-------------------------------------------------------------------------------------

* 第十五章 动态规划（Dynamic Programming）
* 第十六章 贪婪算法（Greedy Algorithms）
* 第十七章 分摊分析（Amortized Analysis）
 
第五部分（Part V） 高级的数据结构（Advanced Data Structures) (6)
-------------------------------------------------------------------------------------

* 第十八章 B-树（B-Trees）
* 第十九章 二项式堆（Binomial Heaps）
* 第二十章 斐波纳契堆（Fibonacci Heaps）
* 第二十一章 不相交集的数据结构（Data Structures for Disjoint Sets）
 
第六部分（Part VI） 图算法（Graph Algorithms）(4)　
-------------------------------------------------------------------------------------

* 第二十二章 基本的图算法（Elementary Graph Algorithms）
* 第二十三章 最小生成树（Minimum Spanning Trees）
* 第二十四章 单源最短路径（Single-Source Shortest Paths）
* 第二十五章 全对的最短路径（All-Pairs Shortest Paths）
* 第二十六章 最大流（Maximum Flow）

第七部分（Part VII） 精选的主题（Selected Topics）　　
-------------------------------------------------------------------------------------

* 第二十七章 排序网络（Sorting Networks）
* 第二十八章 矩阵运算（Matrix Operations）
* 第二十九章 线性规划（Linear Programming）
* 第三十章 多项式与快速傅里叶变换（Polynomials and the FFT）
* 第三十一章 数论算法（Number-Theoretic Algorithms）(5)
* 第三十二章 字符串匹配（String Matching）(5)
* 第三十三章 计算几何学（Computational Geometry）
* 第三十四章 NP-完备性（NP-Completeness）
* 第三十五章 近似算法（Approximation Algorithms）


Language
=========
* c
* c++
* python
* ...
